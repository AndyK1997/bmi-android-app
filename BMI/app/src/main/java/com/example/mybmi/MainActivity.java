package com.example.mybmi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

     RadioButton Mann=null;
     RadioButton Frau=null;
     EditText geburtsdatum=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button berechnenButton =(Button)this.findViewById(R.id.button);
        geburtsdatum= (EditText)this.findViewById(R.id.editTextDate);
        final EditText groesse=(EditText)this.findViewById(R.id.editTextTextPersonName2);
        final EditText gewicht=(EditText)this.findViewById(R.id.inputGewicht);

        Mann=(RadioButton)this.findViewById(R.id.RadioM);
        Frau=(RadioButton)this.findViewById(R.id.RadioW);
        final TextView Erg=(TextView)this.findViewById(R.id.outputView);
        berechnenButton.setOnClickListener( new Button.OnClickListener() {
                                                public void onClick(View v) {

                                                    double BMI = Double.parseDouble(gewicht.getText().toString());
                                                    BMI /=Double.parseDouble(groesse.getText().toString())/100;
                                                    BMI /=Double.parseDouble(groesse.getText().toString())/100;


                                                    String Ausgabe="Der BMI beträgt \t "+BMI+"\n\n\n"+IdealBMI(BMI);
                                                    geschlecht(BMI);
                                                    Erg.setText(Ausgabe);
                                                    Erg.setVisibility(View.VISIBLE);
                                                }
                                            }
            );



    }

    //Create a Toast of BMI Kategorie
    protected void geschlecht(double Value){
        String GewichtsArt=null;

        if(Mann.isChecked()){
            GewichtsArt ="Mann";
            if(Value<20){
                GewichtsArt="Untergewicht";
            }else if(Value>=20 &&Value<=24.999999){
                GewichtsArt="Normalgewicht";
            }else if(Value>=25&&Value<=29.99999){
                GewichtsArt="Übergewicht";
            }else if(Value>=30&&Value<=34.99999){
                GewichtsArt="Starkes Übergewicht";
            }else{
                GewichtsArt="Adipös (du bist zu Fett)";
            }
        }else if(Frau.isChecked()){
            GewichtsArt="Frau";
            if(Value<19){
                GewichtsArt="Untergewicht";
            }else if(Value>=19 &&Value<=23.99999){
                GewichtsArt="Normalgewicht";
            }else if(Value>=24&&Value<=29.999999){
                GewichtsArt="Übergewicht";
            }else if(Value>=30&&Value<=34.999999){
                GewichtsArt="Starkes Übergewicht";
            }else{
                GewichtsArt="Adipös (du bist zu Fett)";
            }
        }
        Toast.makeText(this,GewichtsArt,Toast.LENGTH_LONG).show();
    }

    protected String IdealBMI(double Value){
        String Ideal="Sind nicht im Idealbereich";
        double alter=Double.parseDouble(geburtsdatum.getText().toString());

        if(alter<19 || alter>=19&&alter<=24){
            if(Value>=19 && Value<=24){
                Ideal="Ideal Gewicht BMI(19-24)";
            }
        }else if(alter>=25 && alter <=34){
            if(Value>=20 && Value<=25) {
                Ideal = "Ideal Gewicht BMI(20-25)";
            }
        }else if(alter>=35&&alter <=44){
                if(Value>=21 && Value<=26) {
                    Ideal = "Ideal Gewicht BMI(21-26)";
                }
        }else if(alter>=45 && alter <=54){
            if(Value>=22 && Value<=27) {
                Ideal = "Ideal Gewicht BMI(22-27)";
            }
        }else if(alter>=55 &&alter<=65){
            if(Value>=23 && Value<=28) {
                Ideal = "Ideal Gewicht BMI(23-28)";
            }
        }else if(alter>65){
            if(Value>=24 && Value<=29) {
                Ideal = "Ideal Gewicht BMI(24-29)";
            }
        }
        return  Ideal;
    }


}